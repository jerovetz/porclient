const Container = require('../../src/resources/container').Container
const axios = require('axios')

const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const withSandbox = require('sinon-sandbox-wrapper').withSandbox
const chai = require('chai')
const expect = chai.expect

const expectedContainers = require('../responses/containers.json')
const errorHandler = require('../../src/errorhandler')

chai.use(sinonChai)

describe('Container', () => {
	const host = 'host'
	const token = 'token'

	it('gets containers', withSandbox(async (sandbox) => {
		const getStub = sandbox.stub(axios, 'get')
		const endpoint = 'endpoint'
		getStub.returns({ data: expectedContainers })

		const container = new Container(token, host, endpoint)
		const containers = await container.get()

		expect(containers).to.equal(expectedContainers)
		expect(getStub).calledWith(host + '/api/endpoints/' + endpoint + '/docker/containers/json', { headers: {Authorization: 'Bearer ' + token }})
	}))

	it('stops container', withSandbox(async (sandbox) => {
		const postStub = sandbox.stub(axios, 'post')
		const endpoint = 'endpoint'
		const containerId = 'container_id'

		const container = new Container(token, host, endpoint)
		await container.stop(containerId)

		expect(postStub).calledWith(host + '/api/endpoints/' + endpoint + '/docker/containers/' + containerId + '/stop', {}, { headers: {Authorization: 'Bearer ' + token }})
	}))

	it('removes container', withSandbox(async (sandbox) => {
		const deleteStub = sandbox.stub(axios, 'delete')
		const endpoint = 'endpoint'
		const containerId = 'container_id'

		const container = new Container(token, host, endpoint)
		await container.remove(containerId)

		expect(deleteStub).calledWith(host + '/api/endpoints/' + endpoint + '/docker/containers/' + containerId, { headers: {Authorization: 'Bearer ' + token }})
	}))

	it('creates container with a name', withSandbox(async (sandbox) => {
		const expectedResponse = { data: {Id: "container_id", Warnings: ['just a warning']} }
		const postStub = sandbox.stub(axios, 'post')
		const endpoint = 'endpoint'
		const containerDefinition = JSON.parse('{"container" : "definition"}')
		const name = 'the_container'

		const container = new Container(token, host, endpoint)
		const response = await container.create(containerDefinition, name)

		expect(postStub).calledWith(host + '/api/endpoints/' + endpoint + '/docker/containers/create', 
			containerDefinition,  { headers: {Authorization: 'Bearer ' + token }, params: { name }})
	}))

	it('gets container details', withSandbox(async (sandbox) => {
		const getStub = sandbox.stub(axios, 'get')
		const endpoint = 'endpoint'
		const containerId = 'container_id'
		const expectedContainerData = [ { containerData: 'data '} ]
		getStub.returns({ data: expectedContainerData })

		const container = new Container(token, host, endpoint)
		const containerData = await container.inspect(containerId)

		expect(containerData).to.equal(expectedContainerData)
		expect(getStub).calledWith(host + '/api/endpoints/' + endpoint + '/docker/containers/' + containerId + '/json', { headers: {Authorization: 'Bearer ' + token }})
	}))

})