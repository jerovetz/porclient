const ClientSideError = require('../src/ClientSideError')
const ServerSideError = require('../src/ServerSideError')

const errorHandler = require('../src/errorhandler')
const expect = require('chai').expect

describe('error handler', () => {

	it('throws client side error with proper message', () => {
		const axiosError = new Error('axios error')
		axiosError.response ={ status: 422, statusText: 'unauthorized' }

		try {
			errorHandler(axiosError)
		} catch(e) {
			expect(e).to.be.an.instanceof(ClientSideError)
			expect(e.statusCode).to.eql(422)
			expect(e.message).to.eql('unauthorized')
			return
		}

		expect.fail('an exception was expected.')
	})

	it('throws error, if server side problems occured', () => {
		const axiosError = new Error('axios error')
		axiosError.response ={ status: 500, statusText: 'application error' }

		try {
			errorHandler(axiosError)
		} catch(e) {
			expect(e).to.be.an.instanceof(ServerSideError)
			expect(e.statusCode).to.eql(500)
			expect(e.message).to.eql('application error')
			return
		}

		expect.fail('an exception was expected.')
	})

	it('throws further any other kind of errors', () => {
		const error = new Error('other error')
		try {
			errorHandler(error)
		} catch(e) {
			expect(e).to.be.an.instanceof(Error)
			expect(e.message).to.eql('other error')
			return
		}

		expect.fail('an exception was expected.')
	})

})