const sinon = require('sinon')
const withSandbox = require('sinon-sandbox-wrapper').withSandbox
const axios = require('axios')
const chai = require('chai')
const expect = chai.expect
const sinonChai = require('sinon-chai')

chai.use(sinonChai)

const PortainerApi = require('../src/index')
const ClientSideError =require('../src/ClientSideError')
const ServerSideError =require('../src/ServerSideError')
const Container = require('../src/resources/container').Container

describe('PortainerClient', () => {

	context('authentication', () => {

		it('creates an authenticated stateful api class, if credentials are valid', withSandbox(async (sandbox) => {
			const username = 'user'
			const password = 'pass'
			const host = 'host';
			const token = 'tokentoken'

			const response = { data: { jwt: token }, status: 200 }
			const authStub = sandbox.stub(axios, 'post').returns(response)

			const api = await PortainerApi.Auth(username, password, host)

			expect(api.host).to.eql(host)
			expect(api.token).to.eql(token)
			expect(authStub).calledWith('host/api/auth', { Username: username, Password: password })
		}))

		it('throws error, if credentials are invalid', withSandbox(async (sandbox) => {
			const username = 'user'
			const password = 'invalid_pass'
			const axiosError = new Error('axios error')
			axiosError.response ={ status: 422 }

			const authStub = sandbox.stub(axios, 'post').throws(axiosError)
			try {
				const api = await PortainerApi.Auth(username, password, 'irrelevant')
			} catch(e) {
				expect(authStub).called
				expect(e).to.be.an.instanceof(ClientSideError)
				expect(e.statusCode).to.eql(422)
				return
			}

			expect.fail('an exception was expected.')
		}))

		it('throws error, if server side problems occured', withSandbox(async (sandbox) => {
			const username = 'user'
			const password = 'invalid_pass'
			const axiosError = new Error('axios error')
			axiosError.response ={ status: 500 }

			const authStub = sandbox.stub(axios, 'post').throws(axiosError)
			try {
				const api = await PortainerApi.Auth(username, password, 'irrelevant')
			} catch(e) {
				expect(authStub).called
				expect(e).to.be.an.instanceof(ServerSideError)
				expect(e.statusCode).to.eql(500)
				return
			}

			expect.fail('an exception was expected.')
		}))

		it('throws further any other kind of errors', withSandbox(async (sandbox) => {
			const username = 'user'
			const password = 'invalid_pass'
			const error = new Error('other error')

			const authStub = sandbox.stub(axios, 'post').throws(error)
			try {
				const api = await PortainerApi.Auth(username, password, 'irrelevant')
			} catch(e) {
				expect(authStub).called
				expect(e).to.be.an.instanceof(Error)
				expect(e.message).to.eql('other error')
				return
			}

			expect.fail('an exception was expected.')
		}))
	})

	context('resource get', () => {

		it('gets containers with proper endpoint', withSandbox(async (sandbox) => {
			const username = 'user'
			const password = 'pass'
			const host = 'host';
			const token = 'tokentoken'

			const response = { data: { jwt: token }, status: 200 }
			const authStub = sandbox.stub(axios, 'post').returns(response)

			const api = await PortainerApi.Auth(username, password, host)
			const apiWithContainers = api.getContainersForEndpoint('endpoint')

			expect(api.containers).to.be.an.instanceof(Container)
			expect(api.containers.token).to.eql(token)
			expect(api.containers.host).to.eql(host)
			expect(api.containers.endpoint).to.eql('endpoint')
		}))
	})

})