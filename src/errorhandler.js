const ClientSideError = require('./ClientSideError')
const ServerSideError = require('./ServerSideError')

module.exports = (e) => {
	if (e.response && e.response.status && e.response.status < 500) {
		throw new ClientSideError(e.response.statusText, e.response.status)
	}

	if (e.response && e.response.status && e.response.status >= 500) {
		throw new ServerSideError(e.response.statusText, e.response.status)
	}

	throw e
}