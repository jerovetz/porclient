const axios = require('axios')

const errorHandler = require('./errorhandler')
const RESPONSE_CODE = require('http-response-codes')
const Container = require('./resources/container').Container

module.exports = class PortainerApi {

	constructor(token, host) {
		this.token = token
		this.host = host
	}

	static async Auth(username, password, host) {
		try {
			const response = await axios.post(host + '/api/auth', { Username: username, Password: password })
			return new PortainerApi(response.data.jwt, host)
		} catch (e) {
			errorHandler(e)
		}
	}

	getContainersForEndpoint(endpoint) {
		this.containers = new Container(this.token, this.host, endpoint)
		return this
	}
}