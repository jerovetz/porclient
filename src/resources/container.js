const axios = require('axios')
const errorHandler = require('../errorhandler')

class Container {
	constructor(token, host, endpoint) {
		this.token = token
		this.host = host
		this.endpoint = endpoint
	}

	async get() {
		try {
			const response = await axios.get(this._generateBaseUrl() + 'json', { headers: { Authorization: 'Bearer ' + this.token }})
			return response.data
		} catch(e) {
			errorHandler(e)
		}
	}

	async create(container, name) {
		try {
			const response = await axios.post(this._generateBaseUrl() + 'create', container,
				{ headers: { Authorization: 'Bearer ' + this.token }, params: { name }})
			return response.data
		} catch(e) {
			errorHandler(e)
		}
	}

	async start(containerId) {
		try {
			await axios.post(this._generateBaseUrl() + containerId + '/start', {}, this._createHeaders())
		} catch(e) {
			errorHandler(e)
		}
	}

	async stop(containerId) {
		try {
			await axios.post(this._generateBaseUrl() + containerId + '/stop', {}, this._createHeaders())
		} catch(e) {
			errorHandler(e)
		}
	}

	async remove(containerId) {
		try {
			const response = await axios.delete(this._generateBaseUrl() + containerId, this._createHeaders())
		} catch(e) {
			errorHandler(e)
		}
	}

	async inspect(containerId) {
		try {
			const response = await axios.get(this._generateBaseUrl() + containerId + '/json', this._createHeaders())
			return response.data
		} catch(e) {
			errorHandler(e)
		}
	}

	_createHeaders() {
		return { headers: { Authorization: 'Bearer ' + this.token }}
	}

	_generateBaseUrl() {
		return this.host + '/api/endpoints/' + this.endpoint + '/docker/containers/'
	}

}

module.exports.Container = Container