module.exports = class ServerSideError extends Error {
	
	constructor(message, statusCode) {
		super(message)
		this.name = 'ServerSideError'
		this.statusCode = statusCode
	}
	
}