module.exports = class ClientSideError extends Error {
	
	constructor(message, statusCode) {
		super(message)

		this.name = 'ClientSideError'
		this.statusCode = statusCode
	}
	
}